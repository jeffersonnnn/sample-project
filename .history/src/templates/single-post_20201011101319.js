import React from 'react'
import Layout  from '../components/layout';
import Sidebar from '../components/sidebar'
import { graphql } from 'gastby'

const SinglePost = () => {
    return (
        <Layout>

        </Layout>
    )
}

export const postQuery = graphql`
    query blogPostBySlug($slug: String!) {
        markdownRemark(fields: { path: { eq: $slug }}) {
            id
            html
            frontmatter {
                title
                author
                date(fron)
            }
        }
    }
`

export default SinglePost