import React from 'react'
import Layout  from '../components/layout';
import Sidebar from '../components/sidebar'
import { graphql } from 'gastby'

const SinglePost = () => {
    return (
        <Layout>

        </Layout>
    )
}

export const postQuery = graphql`
    query blogPostBySlug($slug: String!) {
        markdownRemark(fields: { path: { eq: $slug }}) {
            id
            html
            frontmatter {
                title
                author
                date(formatString: "MMM Do YYYY")
                tags
                image {
                    childImageShar
                }
            }
        }
    }
`

export default SinglePost