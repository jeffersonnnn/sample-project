import React from 'react'
import { Link } from 'gatsby'
import { Card, CardTitle, CardText, CardSubtitle, CardBody } from 'reactstrap'

const Post = ({ title, author, path, date, body }) => {
    return (
        <Card>
            <CardBody>
                <CardTitle>
                    {title}
                </CardTitle>
                <CardSubtitle>
                    <span class></span>
                </CardSubtitle>
            </CardBody>
        </Card>
    )
}