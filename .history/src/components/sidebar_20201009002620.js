import React from "react"
import {
  Card,
  CardTitle,
  CardBody,
  Form,
  FormGroup,
  Input,
} from "reactstrap"
import { graphql, StaticQuery } from 'gastby';
import Img from 'gatsby-image';

const Sidebar = () => (
  <div>
    <Card>
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3">
          Newletter
        </CardTitle>
        <Form className="text-center">
          <FormGroup>
            <Input
              type="email"
              name="email"
              placeholder="your email address..."
            />
          </FormGroup>
          <button className="btn btn-outline-success text-uppercase">
            Subscribe
          </button>
        </Form>
      </CardBody>
    </Card>

    <Card>
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3">
          Advertisement
        </CardTitle>
        <img
          src="https://via.placeholder.com/320x200"
          alt="Advert"
          style={{ width: "100%" }}
        />
      </CardBody>
    </Card>

    <Card>
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3">
          Recent Posts
        </CardTitle>
<StaticQuery query={sidebarQuery} render={(data) => (
    <div>
        {data.allMarkdownRemark.edges.map(({ node }) => {
            <Card key={node.id}>
                <
                <Img className="card-image-top" fluid={node.frontmatter.image.childImageSharp.fluid} />
            </Card>
        })}
    </div>
    
)} />
      </CardBody>
    </Card>
  </div>
)

const sidebarQuery = graphql`
    query sidebarQuery {
        allMarkdownRemark (
            sort: { fileds: [frontmatter____date], order: DESC }
            limit: 3
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        path
                        image {
                            childImageSharp {
                                fluid (maxWidth: 300) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`

export default Sidebar
