import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => (
  <Layout pageTitle="About us">
    <SEO title="About" />
    <p>Lorem Ipsum</p>
  </Layout>
)

export default AboutPage
