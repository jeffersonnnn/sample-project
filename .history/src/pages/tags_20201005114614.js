import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const TagsPage = () => (
  <Layout>
    <SEO title="Tags" />
    <h1>Tags Us</h1>
  </Layout>
)

export default TagsPage
