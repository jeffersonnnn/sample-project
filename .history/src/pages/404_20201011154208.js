import React from "react"
import Link from 
import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout pageTitle="Oops, something went wrong...">
    <SEO title="404: Not found" />
    <h1>404: Not Found</h1>
    <Link className="btn btn-primary text-uppercase" to={'/'}></Link>
  </Layout>
)

export default NotFoundPage
